FROM node:12.16.1-alpine as builder
COPY . /apps
WORKDIR /apps

FROM node:12.16.1-alpine as runner
ENV NODE_ENV="production"
WORKDIR /apps
EXPOSE 80

